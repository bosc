#ifndef EXPTYPE_H
#define EXPTYPE_H

#define SHATTER			1
#define EXPLODE_ON_HIT		2
#define FALL			4
#define SMOKE			8
#define FIRE			16
#define BITMAPONLY		32

#define BITMAP1			256
#define BITMAP2			512
#define BITMAP3			1024
#define BITMAP4			2048
#define BITMAP5			4096
#define BITMAPNUKE		8192
#define BITMAPMASK		16128

#define ACTIVATION			1	// set or get
#define STANDINGMOVEORDERS	2	// set or get
#define STANDINGFIREORDERS	3	// set or get
#define HEALTH				4	// get (0-100%)
#define INBUILDSTANCE		5	// set or get
#define BUSY				6	// set or get (used by misc. special case missions like transport ships)
#define PIECE_XZ			7	// get
#define PIECE_Y				8	// get
#define UNIT_XZ				9	// get
#define	UNIT_Y				10	// get
#define UNIT_HEIGHT			11	// get
#define XZ_ATAN				12	// get atan of packed x,z coords
#define XZ_HYPOT			13	// get hypot of packed x,z coords
#define ATAN				14	// get ordinary two-parameter atan
#define HYPOT				15	// get ordinary two-parameter hypot
#define GROUND_HEIGHT		16	// get
#define BUILD_PERCENT_LEFT	17	// get 0 = unit is built and ready, 1-100 = How much is left to build
#define YARD_OPEN			18	// set or get (change which plots we occupy when building opens and closes)
#define BUGGER_OFF			19	// set or get (ask other units to clear the area)
#define ARMORED				20	// set or get

#define IN_WATER            28
#define CURRENT_SPEED       29
//#define MAGIC_DEATH         31
#define VETERAN_LEVEL       32
#define ON_ROAD             34

#define MAX_ID                   70
#define MY_ID                    71
#define UNIT_TEAM                72
#define UNIT_BUILD_PERCENT_LEFT  73
#define UNIT_ALLIED              74
#define MAX_SPEED                75
#define CLOAKED                  76
#define WANT_CLOAK               77
#define GROUND_WATER_HEIGHT      78 // get land height, negative if below water
#define UPRIGHT                  79 // set or get
#define	POW                      80 // get
#define PRINT                    81 // get, so multiple args can be passed
#define HEADING                  82 // get
#define TARGET_ID                83 // get
#define LAST_ATTACKER_ID         84 // get
#define LOS_RADIUS               85 // set or get
#define AIR_LOS_RADIUS           86 // set or get
#define RADAR_RADIUS             87 // set or get
#define JAMMER_RADIUS            88 // set or get
#define SONAR_RADIUS             89 // set or get
#define SONAR_JAM_RADIUS         90 // set or get
#define SEISMIC_RADIUS           91 // set or get
#define DO_SEISMIC_PING          92 // get
#define CURRENT_FUEL             93 // set or get
#define TRANSPORT_ID             94 // get
#define SHIELD_POWER             95 // set or get

// Indices for SET, GET, and GET_UNIT_VALUE for LUA return values
#define LUA0 110 // (LUA0 returns the lua call status, 0 or 1)
#define LUA1 111
#define LUA2 112
#define LUA3 113
#define LUA4 114
#define LUA5 115
#define LUA6 116
#define LUA7 117
#define LUA8 118
#define LUA9 119


#endif // EXPTYPE_H
