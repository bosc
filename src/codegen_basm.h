#ifndef CODEGEN_BASM_H
#define CODEGEN_BASM_H

#include <string>
#include <sstream>
#include <stack>

#include "codegen.h"
#include "ast.h"
#include "compiler.h"


class CodeGeneratorBasm : public CodeGeneratorBase {
protected:
    struct stackval {
        std::string str;
        int ival;
        stackval(int i):ival(i) {}
        stackval(std::string s):str(s) {}
        stackval(std::string s, int i):str(s), ival(i) {}
    };
    std::stringstream code;
    std::stack<stackval> stack;
    function_info *curFunc;

    enum Vartype { VAR_LOCAL, VAR_STATIC, VAR_PIECE, VAR_FUNC, VAR_NOTFOUND };
    Vartype getVarType(std::string name)
    {   if (curFunc && curFunc->locals.find(name) != curFunc->locals.end())
            return VAR_LOCAL;
        else if (pinfo.staticvars.find(name) != pinfo.staticvars.end())
            return VAR_STATIC;
        else if (pinfo.pieces.find(name) != pinfo.pieces.end())
            return VAR_PIECE;
        else if (pinfo.functions.find(name) != pinfo.functions.end())
            return VAR_FUNC;
        else
            return VAR_NOTFOUND;
    }

    bool returned;
    int inExpr;
    bool inCall;
    int pushes, pops;

    void emitBinOp(BinaryOperator* binop);

public:
    CodeGeneratorBasm(parse_info &pi):
        CodeGeneratorBase(pi), curFunc(0), returned(false),
        inExpr(0), pushes(0), pops(0) {}
    void init();
    void emitOpCode(OpcodeVal op);
    void emitInt32(int v);
    void emitPiece(std::string name);
    void emitAST(ASTNode* ast);
    virtual std::string getCode() const
    { return code.str(); }
};

#endif
