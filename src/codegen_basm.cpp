#include "ast.h"
#include "codegen.h"
#include "codegen_basm.h"
#include "utils.h"

void CodeGeneratorBasm::init()
{
    size_t counter = 0;

    if (!pinfo.pieces.empty()) {
        code << "piece ";
        FOREACH(SymbolTable, pinfo.pieces, it) {
            ++counter;
            code << it->first;
            if (counter == pinfo.pieces.size()) code << ";" << std::endl;
            else code << ",";
        }
    }
    if (!pinfo.staticvars.empty()) {
        counter = 0;
        code << "static-var ";
        FOREACH(SymbolTable, pinfo.staticvars, it) {
            ++counter;
            code << it->first;
            if (counter == pinfo.staticvars.size()) code << ";" << std::endl;
            else code << ",";
        }
    }
}

#define EOL ";\n"

void CodeGeneratorBasm::emitOpCode(OpcodeVal op)
{
    switch (op) {
    case OP_PUSH_CONSTANT:
        assert(!stack.empty());
        code << "\tpushc " << stack.top().ival << EOL;
        stack.pop();
        ++pushes;
        break;
    case OP_PUSH_LOCAL_VAR:
        assert(!stack.empty());
        code << "\tpushl " << stack.top().str << EOL;
        stack.pop();
        ++pushes;
        break;
    case OP_PUSH_STATIC:
        assert(!stack.empty());
        code << "\tpushs " << stack.top().str << EOL;
        stack.pop();
        ++pushes;
        break;
    case OP_POP_STACK:
        code << "\tpop" << EOL;
        ++pops;
        break;
    case OP_POP_LOCAL_VAR:
        assert(!stack.empty());
        code << "\tpopl " << stack.top().str << EOL;
        stack.pop();
        ++pops;
        break;
    case OP_POP_STATIC:
        assert(!stack.empty());
        code << "\tpops " << stack.top().str << EOL;
        stack.pop();
        ++pops;
        break;
    case OP_RETURN:
        code << "\tret" << EOL;
        ++pops;
        break;
    case OP_CALL:
        code << "\tcall" << EOL
            << "\tddw " << stack.top().str << EOL
            << "\tddw " << stack.top().ival << EOL;
        // calls pop all their arguments and push a return value on the stack
        pops += stack.top().ival;
        ++pushes;
        stack.pop();
        break;
    case OP_ADD:
    case OP_SUB:
    case OP_MUL:
    case OP_DIV:
        switch(op) {
        case OP_ADD: code << "\tadd" << EOL; break;
        case OP_SUB: code << "\tsub" << EOL; break;
        case OP_MUL: code << "\tmul" << EOL; break;
        case OP_DIV: code << "\tdiv" << EOL; break;
        default: LOG("unknown op: %d\n", op);
        }
        pops += 2;
        ++pushes;
        break;
    default:
        break;
    }   
}


void CodeGeneratorBasm::emitInt32(int i)
{
    code << "\tddw " << i << EOL;
}


void CodeGeneratorBasm::emitAST(ASTNode* ast)
{
    if (ast->getType() == std::string("Number")) {
        if (!inExpr) {
            LOGWARNING(ast, "expression has no effect\n");
            return;
        }
        Number* n = dynamic_cast<Number*>(ast);
        assert(n);
        stack.push((int)n->val);
        emitOpCode(OP_PUSH_CONSTANT);
        return;
    } else if (ast->getType() == std::string("Ident")) {
        if (!curFunc)
            return;
        if (!inExpr) {
            LOGWARNING(ast, "expression has no effect\n");
            return;
        }
        Ident* id = dynamic_cast<Ident*>(ast);
        assert(id);
        // emit value
        stack.push(id->name);
        switch (getVarType(id->name)) {
            case VAR_LOCAL:  emitOpCode(OP_PUSH_LOCAL_VAR); break;
            case VAR_STATIC: emitOpCode(OP_PUSH_STATIC); break;
            case VAR_PIECE:  emitOpCode(OP_PUSH_CONSTANT); break;
            case VAR_FUNC:   LOGERROR(ast, "invalid usage of function \"%s\"\n", id->name.c_str());
                             ++pinfo.errors;
                             break;
            default: LOGERROR(id, "unknown identifier \"%s\"\n", id->name.c_str());
                     ++pinfo.errors;
                     break;
        }
        return;
    } else if (ast->getType() == std::string("CallExpr")
            || ast->getType() == std::string("Call")) {
        std::string name;
        ASTNode* args;
        if (ast->getType() == std::string("Call")) {
            Call* c = dynamic_cast<Call*>(ast);
            assert(c);
            name = c->name;
            args = c->argv;
        } else {
            CallExpr* c = dynamic_cast<CallExpr*>(ast);
            assert(c);
            Ident* id = dynamic_cast<Ident*>(c->call);
            assert(id);
            name = id->name;
            args = c->args;
        }
        if (args) {
            ++inExpr;
            emitAST(args);
            --inExpr;
        }
        int argc;
        if (!args)
            argc = 0;
        else if (args->getType() == std::string("List"))
            argc = args->getChildren().size();
        else
            argc = 1;
        stack.push(stackval(name, argc));
        emitOpCode(OP_CALL);
        return;
    } else if (ast->getType() == std::string("Assign")) {
        if (!curFunc) {
            LOGWARNING(ast, "variables can't be initialized out of functions\n");
            return;
        }
        Assign* as = dynamic_cast<Assign*>(ast);
        assert(as);
        Ident* id = dynamic_cast<Ident*>(as->left);
        assert(id);
        assert(find_symbol(pinfo, id->name)
                || find_local_symbol(pinfo, curFunc->name, id->name));
        ++inExpr;
        emitAST(as->right);
        --inExpr;
        stack.push(id->name);
        switch (getVarType(id->name)) {
            case VAR_LOCAL:  emitOpCode(OP_POP_LOCAL_VAR); break;
            case VAR_STATIC: emitOpCode(OP_POP_STATIC); break;
            case VAR_PIECE:  LOGERROR(id, "cannot assign to piece\n");
                             ++pinfo.errors;
                             break;
            case VAR_FUNC:   LOGERROR(id, "cannot assign to function\n");
                             ++pinfo.errors;
                             break;
            default: LOGERROR(id, "unknown identifier \"%s\"\n", id->name.c_str());
                     ++pinfo.errors;
                     break;
        }
        return;
    } else if (ast->getType() == std::string("Function")) {
        Function* f = dynamic_cast<Function*>(ast);
        assert(f);
        FunctionProto* fp = dynamic_cast<FunctionProto*>(f->proto);
        assert(fp);
        curFunc = &pinfo.functions[fp->name];
        // generate function preamble
        code << "\n\n";
        code << "proc " << fp->name << "\n";
        returned = false;
        inExpr = 0;
        
        // output argument and variable declarations
        NodeTable used;
        if(!curFunc->params.empty()) {
            size_t counter = 0;
            code << "args ";
            FOREACH(function_info::params_type, curFunc->params, it) {
                ++counter;
                code << *it;
                if (counter == curFunc->params.size())
                    code << EOL;
                else
                    code << ", ";
                used.insert(NodeTable::value_type(*it, 0));
            }
        } else {
            code << "args none" << EOL;
        }
        if (curFunc->locals.size() - curFunc->params.size() == 0) {
            code << "vars none" << EOL;
        } else {
            code << "vars ";
            size_t counter = curFunc->params.size();
            FOREACH(SymbolTable, curFunc->locals, it) {
                ++counter;
                if (used.find(it->first) != used.end())
                    continue;
                code << it->first;
                if (counter == curFunc->locals.size())
                    code << EOL;
                else
                    code << ", ";
            }
        }

        if (pushes != pops) {
            LOGWARNING(f, "pushes != pops: %d push(es), %d pop(s)\n", pushes, pops);
        }
        code << "beginproc\n";
        emitAST(f->instr);
        if (!returned)
            code << "\tpushc 0" << EOL << "\tret" << EOL;
        code << "endproc" << EOL;
        curFunc = 0;
        returned = false;
        inExpr = 0;
        return;
    } else if (ast->getType() == std::string("Return")) {
        Return* ret = dynamic_cast<Return*>(ast);
        assert(ret);
        if (!ret->retval) {
            code << "\tpushc 0" << EOL;
            ++pushes;
        }
        else {
            ++inExpr;
            emitAST(ret->retval);
            --inExpr;
        }
        emitOpCode(OP_RETURN);
        returned = true;
        return;
    } else if (ast->getType() == std::string("Declaration")) {
        // no-op
        return;
    } else if (dynamic_cast<BinaryOperator*>(ast)) {
        emitBinOp((BinaryOperator*)ast);
        return;
    }
    CodeGeneratorBase::emitAST(ast);
}

void CodeGeneratorBasm::emitBinOp(BinaryOperator* binop)
{
    emitAST(binop->left);
    emitAST(binop->right);
    std::string s = binop->getType();
    if (s == "Add") {
        emitOpCode(OP_ADD);
    } else if (s == "Subtract") {
        emitOpCode(OP_SUB);
    } else if (s == "Multiply") {
        emitOpCode(OP_MUL);
    } else if (s == "Divide") {
        emitOpCode(OP_DIV);
    }
}
