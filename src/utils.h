#ifndef UTILS_H
#define UTILS_H

#define FOREACH(conttype, container, iter) \
    for(conttype::iterator iter = (container).begin(); \
            iter != (container).end(); \
            ++iter)


#endif
