#ifndef CODEGEN_H
#define CODEGEN_H

#include <map>
#include <string>
#include <cassert>

#include "ast.h"
#include "compiler.h"

//Command documentation from http://visualta.tauniverse.com/Downloads/cob-commands.txt
//And some information from basm0.8 source (basm ops.txt)

enum OpcodeVal {
    //Model interaction
    OP_MOVE       = 0x10001000,
    OP_TURN       = 0x10002000,
    OP_SPIN       = 0x10003000,
    OP_STOP_SPIN  = 0x10004000,
    OP_SHOW       = 0x10005000,
    OP_HIDE       = 0x10006000,
    OP_CACHE      = 0x10007000,
    OP_DONT_CACHE = 0x10008000,
    OP_MOVE_NOW   = 0x1000B000,
    OP_TURN_NOW   = 0x1000C000,
    OP_SHADE      = 0x1000D000,
    OP_DONT_SHADE = 0x1000E000,
    OP_EMIT_SFX   = 0x1000F000,

    //Blocking operations
    OP_WAIT_TURN  = 0x10011000,
    OP_WAIT_MOVE  = 0x10012000,
    OP_SLEEP      = 0x10013000,

    //Stack manipulation
    OP_PUSH_CONSTANT    = 0x10021001,
    OP_PUSH_LOCAL_VAR   = 0x10021002,
    OP_PUSH_STATIC      = 0x10021004,
    OP_CREATE_LOCAL_VAR = 0x10022000,
    OP_POP_LOCAL_VAR    = 0x10023002,
    OP_POP_STATIC       = 0x10023004,
    OP_POP_STACK        = 0x10024000,		// Not sure what this is supposed to do

    //Arithmetic operations
    OP_ADD         = 0x10031000,
    OP_SUB         = 0x10032000,
    OP_MUL         = 0x10033000,
    OP_DIV         = 0x10034000,
    OP_BITWISE_AND = 0x10035000,
    OP_BITWISE_OR  = 0x10036000,
    OP_BITWISE_XOR = 0x10037000,
    OP_BITWISE_NOT = 0x10038000,

    //Native function calls
    OP_RAND           = 0x10041000,
    OP_GET_UNIT_VALUE = 0x10042000,
    OP_GET            = 0x10043000,

    //Comparison
    OP_SET_LESS             = 0x10051000,
    OP_SET_LESS_OR_EQUAL    = 0x10052000,
    OP_SET_GREATER          = 0x10053000,
    OP_SET_GREATER_OR_EQUAL = 0x10054000,
    OP_SET_EQUAL            = 0x10055000,
    OP_SET_NOT_EQUAL        = 0x10056000,
    OP_LOGICAL_AND          = 0x10057000,
    OP_LOGICAL_OR           = 0x10058000,
    OP_LOGICAL_XOR          = 0x10059000,
    OP_LOGICAL_NOT          = 0x1005A000,

    //Flow control
    OP_START           = 0x10061000,
    OP_CALL            = 0x10062000, // converted when executed
    OP_REAL_CALL       = 0x10062001, // spring custom
    OP_LUA_CALL        = 0x10062002, // spring custom
    OP_JUMP            = 0x10064000,
    OP_RETURN          = 0x10065000,
    OP_JUMP_NOT_EQUAL  = 0x10066000,
    OP_SIGNAL          = 0x10067000,
    OP_SET_SIGNAL_MASK = 0x10068000,

    //Piece destruction
    OP_EXPLODE    = 0x10071000,
    OP_PLAY_SOUND = 0x10072000,

    //Special functions
    OP_SET    = 0x10082000,
    OP_ATTACH = 0x10083000,
    OP_DROP   = 0x10084000,

    //other values
    UNKNOWN_OPCODE = 0xffffffff,
};


class CodeGeneratorBase {
protected:
    void fillOpcodeMap();
    typedef std::map<std::string, OpcodeVal> OpcodeMap;
    OpcodeMap opcodeMap;
    OpcodeVal getOpcodeForTAOp(std::string taopname)
    { return opcodeMap["oper:"+taopname]; }
public:
    parse_info& pinfo;
    CodeGeneratorBase(parse_info& pi): pinfo(pi) { fillOpcodeMap(); }

    /// initalize emission
    virtual void init() = 0;
    /// emit single opcode
    virtual void emitOpCode(OpcodeVal op) = 0;
    /// emit single int
    virtual void emitInt32(int i) = 0;
    /// emit code for an ast expression
    virtual void emitAST(ASTNode* ast);
    /// returns opcode that does what the node does
    OpcodeVal getOpcodeForAST(ASTNode* node)
    {
        assert(node);
        if (node->getType() == std::string("TAOperation")) {
            TAOperation* ta = dynamic_cast<TAOperation*>(node);
            assert(ta);
            return getOpcodeForTAOp(ta->name);
        }
        std::string k(node->getType());
        if (opcodeMap.find(k) != opcodeMap.end())
            return opcodeMap[std::string(node->getType())];
        return UNKNOWN_OPCODE;
    }

    virtual std::string getCode() const = 0;
};

#endif
