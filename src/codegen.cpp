#include "codegen.h"
#include "utils.h"


void CodeGeneratorBase::fillOpcodeMap()
{
    opcodeMap["Number"] = OP_PUSH_CONSTANT;

    opcodeMap["Add"] = OP_ADD;
    opcodeMap["Subtract"] = OP_SUB;
    opcodeMap["Multiply"] = OP_MUL;
    opcodeMap["Divide"] = OP_DIV;
    opcodeMap["BitwiseNot"] = OP_BITWISE_NOT;
    opcodeMap["BitwiseAnd"] = OP_BITWISE_AND;
    opcodeMap["BitwiseOr"] = OP_BITWISE_OR;
    opcodeMap["BitwiseXor"] = OP_BITWISE_XOR;

    opcodeMap["oper:turn"] = OP_TURN;
    opcodeMap["oper:turn-now"] = OP_TURN_NOW;
    opcodeMap["oper:sleep"] = OP_SLEEP;
    opcodeMap["oper:move"] = OP_MOVE;
    opcodeMap["oper:move-now"] = OP_MOVE_NOW;
    opcodeMap["oper:spin"] = OP_SPIN;
    opcodeMap["oper:stop-spin"] = OP_STOP_SPIN;
    opcodeMap["oper:wait-for-move"] = OP_WAIT_MOVE;
    opcodeMap["oper:wait-for-turn"] = OP_WAIT_TURN;
    opcodeMap["oper:shade"] = OP_SHADE;
    opcodeMap["oper:dont-shade"] = OP_SHADE;
    opcodeMap["oper:show"] = OP_SHOW;
    opcodeMap["oper:hide"] = OP_HIDE;
    opcodeMap["oper:rand"] = OP_RAND;
    opcodeMap["oper:signal"] = OP_SIGNAL;
    opcodeMap["oper:set-signal-mask"] = OP_SET_SIGNAL_MASK;
    opcodeMap["oper:explode"] = OP_EXPLODE;
    opcodeMap["oper:emit-sfx"] = OP_EMIT_SFX;
    opcodeMap["oper:attach-unit"] = OP_ATTACH;
    opcodeMap["oper:drop-unit"] = OP_DROP;
    opcodeMap["GetNoArgs"] = OP_GET_UNIT_VALUE;
    opcodeMap["Get"] = OP_GET;
    opcodeMap["Set"] = OP_SET;

    opcodeMap["LessThan"] = OP_SET_LESS;
    opcodeMap["GreaterThan"] = OP_SET_GREATER;
    opcodeMap["LessEqual"] = OP_SET_LESS_OR_EQUAL;
    opcodeMap["GreaterEqual"] = OP_SET_GREATER_OR_EQUAL;
    opcodeMap["Equal"] = OP_SET_EQUAL;
    opcodeMap["NotEqual"] = OP_SET_NOT_EQUAL;


    opcodeMap["Jump"] = OP_JUMP;
    opcodeMap["JumpIfZero"] = OP_JUMP_NOT_EQUAL;
    opcodeMap["Return"] = OP_RETURN;
    opcodeMap["Call"] = OP_CALL;
    opcodeMap["CallCOB"] = OP_REAL_CALL;
    opcodeMap["CallLua"] = OP_LUA_CALL;

    /*
    opcodeMap[""] = ;
    opcodeMap[""] = ;
    opcodeMap[""] = ;
    opcodeMap[""] = ;
    opcodeMap[""] = ;
    opcodeMap[""] = ;
    opcodeMap[""] = ;
    */
}


void CodeGeneratorBase::emitAST(ASTNode* ast)
{
    assert(ast);

    NodeStore v = ast->getChildren();
    for (NodeStore::iterator it = v.begin(); it != v.end(); ++it) {
        if (*it)
            emitAST(*it);
    }
}

