#ifndef COMPILER_H
#define COMPILER_H

#include <vector>
#include <map>
#include "ast.h"

/// ident node and its index
typedef std::pair<Ident*, int> Symbol;
typedef std::map<std::string, Symbol> SymbolTable;
typedef std::map<std::string, Label*> LabelTable;
typedef std::map<std::string, ASTNode*> NodeTable;

struct parse_info;

struct function_info
{
    /// index in table
    int index;
    /// function name
    std::string name;
    /// function's AST
    Function* ast;

    /// param store type
    typedef std::vector<std::string> params_type;
    /// parameters by name
    params_type params;
    /// reads parameters from passed in node (or this->ast if paramNode == 0)
    int readParams(parse_info& pi, ASTNode* paramNode = 0);

    /// function local variables
    SymbolTable locals;

    function_info(): index(-1), ast(0) {}
};

typedef std::map<std::string, function_info> FunctionTable;

struct parse_info
{
    /// total errors
    int errors;
    /// static-vars
    SymbolTable staticvars;
    /// pieces
    SymbolTable pieces;
    /// functions
    FunctionTable functions;
    /// functions called, but undefined at the place of call
    NodeTable undefinedFuncs;
    /// labels
    LabelTable labels;
    /// current locals
    SymbolTable *locals;

    parse_info(): errors(0), locals(0) {}
};

/// simplifies semicolon trees into blocks and comma trees into lists
void simplify_ast(ASTNode* root);

/// checks tree parents
bool tree_check_parents(ASTNode* root);

/// validates gotos, returns, continues, breaks
int validate_tree(parse_info &pi, ASTNode* root);

/// checks if validate tree didn't leave something behind
int check_parse_results(parse_info &pi);

/// constant expression precalculation
void constant_expression_optimization(ASTNode* root);

/// find symbol of a given name in a given parse_info object
ASTNode* find_symbol(parse_info &pi, std::string name);

/// find symbol of a given name in a given function's locals table
ASTNode* find_local_symbol(parse_info& pi, std::string funcname, std::string symname);

#endif
