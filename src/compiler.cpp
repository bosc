#include <string>
#include <stack>
#include <cassert>
#include <stdexcept>

#include "ast.h"
#include "compiler.h"

void simplify_ast(ASTNode *root)
{
    NodeStore v = root->getChildren();
    for (NodeStore::iterator it = v.begin(); it != v.end(); ++it) {
        ASTNode *node = *it;
        if (!node) continue;
        if (node->getType() == std::string("Semicolon")) {
            Semicolon *semi = dynamic_cast<Semicolon*>(node);
            assert(semi);
            semi->parent = NULL;
            Block *block = new Block();
            block->parent = root;
            block->loc = semi->loc;
            semi->traverseTree(block->list, semi);
            for (NodeStore::iterator it2 = block->list.begin(); it2 != block->list.end(); ++it2)
                (*it2)->parent = block;
            *it = block;
            delete semi;
        } else if (node->getType() == std::string("Comma")) {
            Comma* comma = dynamic_cast<Comma*>(node);
            assert(comma);
            comma->parent = NULL;
            List *block = new List();
            block->parent = root;
            block->loc = comma->loc;
            comma->traverseTree(block->list, comma);
            for (NodeStore::iterator it2 = block->list.begin(); it2 != block->list.end(); ++it2)
                (*it2)->parent = block;
            *it = block;
            delete comma;
        }
        simplify_ast(*it);
    }
    root->setChildren(v);
}


bool tree_check_parents(ASTNode* root)
{
    NodeStore v = root->getChildren();
    for (NodeStore::iterator it = v.begin(); it != v.end(); ++it) {
        if (!(*it))
            continue;
        if ((*it)->parent != root) {
            LOG("error:%s: failed check: %s->%s\n", root->locStr().c_str(),
                    root->getType(), (*it)->getType());
            return false;
        }
        if (!tree_check_parents(*it))
            return false;
    }
    return true;
}

static inline ASTNode* find_parent(ASTNode* p, std::string type)
{
    while (p && p->getType() != type)
        p = p->parent;
    return p;
}

static inline NodeStore find_all_children(ASTNode* p, std::string type)
{
    NodeStore r, v = p->getChildren();
    for (NodeStore::iterator it = v.begin(); it != v.end(); ++it) {
        if (!*it)
            continue;
        ASTNode *node = *it;
        if (node->getType() == type)
            r.push_back(node);
        NodeStore tmp = find_all_children(node, type);
        r.insert(r.end(), tmp.begin(), tmp.end());
    }
    return r;
}

ASTNode* find_symbol(parse_info &pi, std::string name)
{
    {
        SymbolTable::iterator it = pi.staticvars.find(name);
        if (it != pi.staticvars.end())
            return it->second.first;

        it = pi.pieces.find(name);
        if (it != pi.pieces.end())
            return it->second.first;

        if (pi.locals) {
            it = pi.locals->find(name);
            if (it != pi.locals->end())
                return it->second.first;
        }
    }

    {
        FunctionTable::iterator it = pi.functions.find(name);
        if (it != pi.functions.end())
            return it->second.ast;
    }
    return 0;
}

ASTNode* find_local_symbol(parse_info& pi, std::string funcname, std::string symname)
{
    FunctionTable::iterator it = pi.functions.find(funcname);
    SymbolTable::iterator symit = it->second.locals.find(symname);
    if (symit != it->second.locals.end())
        return symit->second.first;
    else
        return 0;
}

static bool validate_loop_exit(ASTNode *leaf)
{
    ASTNode *p = find_parent(leaf, "Loop");
    if (p)
        LOG("stopped at %s\n", p->getType());
    else
        LOGERROR(leaf, "%s not in loop\n", leaf->getType());
    return p != NULL;
}

static bool validate_declarations(ASTNode *node)
{
    Declaration *n = dynamic_cast<Declaration*>(node);
    assert(n);
    Typename *t = dynamic_cast<Typename*>(n->type);
    assert(t);
    if (t->name == "var") {
        if (!find_parent(node, "Function")) {
            LOGERROR(t, "local var declaration when not in function\n");
            return false;
        }
    } else if (t->name == "static-var" || t->name == "piece") {
        if (find_parent(node, "Function")) {
            LOGERROR(t, "global declaration when in function\n");
            return false;
        }
    } else {
        LOGERROR(t, "unknown type: %s\n", t->name.c_str());
        return false;
    }
    return true;
}


bool validate_jump(ASTNode *node)
{
    int labelnum = 0;
    Jump *j = dynamic_cast<Jump*>(node);
    assert(j);
    Function *fun = dynamic_cast<Function*>(find_parent(node, "Function"));
    assert(fun);
    NodeStore labels = find_all_children(fun, "Label");
    Label *prev = NULL;
    for (NodeStore::iterator it = labels.begin(); it != labels.end(); ++it) {
        Label *l = dynamic_cast<Label*>(*it);
        assert(l);
        if (l->name == j->dest) {
            ++labelnum;
            if (labelnum >= 2) {
                LOGERROR(j, "%s - duplicate destination: \"%s\"\n", j->getType(), j->dest.c_str());
            }
        }
        prev = l;
    }
    if (labelnum == 0)
        LOGERROR(j, "label \"%s\" not found in function %s\n", j->dest.c_str(), fun->getFunctionName().c_str());
    return labelnum == 1;
}

static bool insert_into_symtable(parse_info& pi, SymbolTable *tbl, Ident *id)
{
    assert(tbl);
    assert(id);
    ASTNode* tmp = find_symbol(pi, id->name);
    if (tmp) {
        LOGERROR(id, "duplicate declaration of \"%s\"\n",
                id->name.c_str());
        LOGLOC(tmp, "previously defined here\n");
        return false;
    } else {
        tbl->insert(SymbolTable::value_type(id->name, Symbol(id, tbl->size())));
        return true;
    }
}


static int insert_many_into_symtable(parse_info& pi, SymbolTable& sym, ASTNode *paramNode)
{
    if (!paramNode)
        return 0;

    Ident *id = dynamic_cast<Ident*>(paramNode);
    if (id) {
        return !insert_into_symtable(pi, &sym, id);
    }

    Assign *as = dynamic_cast<Assign*>(paramNode);
    if (as) {
        id = dynamic_cast<Ident*>(as->left);
        assert(id);
        return !insert_into_symtable(pi, &sym, id);
    }
    
    int errors = 0;
    List *li = dynamic_cast<List*>(paramNode);
    if (li) {
        for (NodeStore::iterator it = li->list.begin(); it != li->list.end(); ++it) {
            as = dynamic_cast<Assign*>(*it);
            if (as)
                id = dynamic_cast<Ident*>(as->left);
            else
                id = dynamic_cast<Ident*>(*it);
            assert(id);
            errors += !insert_into_symtable(pi, &sym, id);
        }
        return errors;
    }

    EPRINTF("bad arguments: type %s\n", paramNode->getType());
    assert(0 && "bad arguments");
    return 666;
}

static bool add_declarations_to_symtable(parse_info& pi, ASTNode* node)
{
    Declaration *decl = dynamic_cast<Declaration*>(node);
    assert(decl);
    Typename *type = dynamic_cast<Typename*>(decl->type);
    assert(type);
    SymbolTable* tbl;
    if (type->name == "piece")
        tbl = &pi.pieces;
    else if (type->name == "static-var")
        tbl = &pi.staticvars;
    else if (type->name == "var")
        return true;
    else {
        LOGERROR(type, "unknown type %s\n", type->name.c_str());
        return false;
    }

    return !insert_many_into_symtable(pi, *tbl, decl->list);
}

static bool add_label(parse_info &pi, ASTNode* node)
{
    Label* label = dynamic_cast<Label*>(node);
    assert(label);
    LabelTable::iterator it = pi.labels.find(label->name);
    if (it == pi.labels.end()) {
        pi.labels.insert(LabelTable::value_type(label->name, label));
        return true;
    }
    else {
        assert(it != pi.labels.end());
        assert(it->second);
        LOGERROR(label, "duplicate label \"%s\"\n", label->name.c_str());
        LOGLOC(it->second, "first declared here\n");
        return false;
    }
}



int function_info::readParams(parse_info& pi, ASTNode *paramNode)
{
    int errors = insert_many_into_symtable(pi, locals, paramNode);
    params.resize(locals.size());
    for (SymbolTable::iterator it = locals.begin(); it != locals.end(); ++it)
        params[it->second.second] = it->first;
    return errors;
}

static double calculate_binop_val(BinaryOperator* binop)
{
    Number* l = dynamic_cast<Number*>(binop->left);
    Number* r = dynamic_cast<Number*>(binop->right);
    assert(l && r);
    std::string name = binop->getType();
    if (name == "Add")
        return l->val + r->val;
    else if (name == "Subtract")
        return l->val - r->val;
    else if (name == "Multiply")
        return l->val * r->val;
    else if (name == "Divide")
        return l->val / r->val;
    else if (name == "ShiftLeft")
        return (int)(l->val) << (int)(r->val);
    else if (name == "ShiftRight")
        return (int)(l->val) >> (int)(r->val);
    else if (name == "BitwiseAnd")
        return (int)(l->val) & (int)(r->val);
    else if (name == "BitwiseOr")
        return (int)(l->val) | (int)(r->val);
    else if (name == "BitwiseXor")
        return (int)(l->val) ^ (int)(r->val);
    else if (name == "LogicalAnd")
        return (bool)(l->val) && (bool)(r->val);
    else if (name == "LogicalOr")
        return (bool)(l->val) && (bool)(r->val);
    else if (name == "LessThan")
        return l->val < r->val;
    else if (name == "LessEqual")
        return l->val <= r->val;
    else if (name == "GreaterEqual")
        return l->val >= r->val;
    else if (name == "GreaterThan")
        return l->val >= r->val;
    else if (name == "Equal")
        return l->val == r->val;
    else if (name == "NotEqual")
        return l->val != r->val;
    else {
        LOGERROR(binop, "unknown binary operator in constant folding: %s\n", binop->getType());
        throw std::runtime_error("unknown binary operator in constant folding");
    }
}

static double calculate_unaop_val(UnaryOperator* unaop)
{
    Number* n = dynamic_cast<Number*>(unaop->operand);
    assert(n);
    std::string name = unaop->getType();
    if (name == "Increment")
        return n->val+1;
    else if (name == "Decrement")
        return n->val-1;
    else if (name == "Negation")
        return -n->val;
    else if (name == "LogicalNot")
        return !(bool)n->val;
    else if (name == "BitwiseNot")
        return ~(int)n->val;
    else {
        LOGERROR(unaop, "unknown unary operator in constant folding: %s\n", unaop->getType());
        throw std::runtime_error("unknown unary operator in constant folding");
    }
}

static ASTNode* constant_expression_fold(ASTNode* node)
{
    assert(node);
    BinaryOperator *binop = dynamic_cast<BinaryOperator*>(node);
    UnaryOperator *unaop = dynamic_cast<UnaryOperator*>(node);
    if (!binop && !unaop)
        return node;

    if (binop) {
        assert(binop->left);
        assert(binop->right);
        if (binop->left->getType() == std::string("Number")) {
            if (binop->right->getType() == std::string("Number")) {
                return new Number(calculate_binop_val(binop));
            } else {
                ASTNode *newnode = constant_expression_fold(binop->right);
                newnode->parent = node;
                binop->right = newnode;
                if (newnode->getType() == std::string("Number"))
                    return new Number(calculate_binop_val(binop));
                else {
                    return binop;
                }
            }
        }
        else if (binop->right->getType() == std::string("Number")) {
            // optimize first
            binop->left = constant_expression_fold(binop->left);
            binop->left->parent = binop;
            // left is not a number
            BinaryOperator *l = dynamic_cast<BinaryOperator*>(binop->left);
            // FIXME this sucks
            if (l && l->right->getType() == std::string("Number")
                    && (l->oper == binop->oper
                        || (l->oper == "-" && binop->oper == "+")
                        || (l->oper == "+" && binop->oper == "-")
                        || (l->oper == "*" && binop->oper == "/")
                        || (l->oper == "/" && binop->oper == "*"))
                    && (l->getType() == std::string("Add")
                        || l->getType() == std::string("Multiply")
                        || l->getType() == std::string("Subtract")
                        || l->getType() == std::string("Divide")
                        || l->getType() == std::string("BitwiseOr")
                        || l->getType() == std::string("BitwiseAnd")
                        || l->getType() == std::string("LogicalAnd")
                        || l->getType() == std::string("LogicalOr"))) {
                // cut out a node
                l->left->parent = binop;
                binop->left = l->left;
                // prepare a node for computation
                l->left = binop->right;
                if (l->getType() == std::string("Subtract")
                        || l->getType() == std::string("Divide")) {
                    // case 1. invert left child and compute
                    BinaryOperator* l2 = l->invert();
                    binop->right = new Number(calculate_binop_val(l2));
                    delete l2;
                } else if (binop->getType() == std::string("Subtract")) {
                    // case 2a. invert self and compute
                    Number* n = dynamic_cast<Number*>(binop->right);
                    assert(n);
                    n->val = -n->val;
                    binop->right = new Number(calculate_binop_val(l));
                } else if (binop->getType() == std::string("Divide")) {
                    // case 2b. invert self and compute
                    Number* n = dynamic_cast<Number*>(binop->right);
                    assert(n);
                    n->val = 1./n->val;
                    binop->right = new Number(calculate_binop_val(l));
                }
                else {
                    // case 3. no need to invert anything, same operators
                    binop->right = new Number(calculate_binop_val(l));
                }
                delete l->right;
                delete l->left;
                delete l;
                binop->right->parent = binop;
                // scary, but works -- tree is shortening with every call
                return constant_expression_fold(binop);
            }
            else {
                // nothing scary to do
                if (binop->left->getType() == std::string("Number"))
                    return new Number(calculate_binop_val(binop));
                else {
                    return binop;
                }
            }
        } else {
            binop->left = constant_expression_fold(binop->left);
            binop->right = constant_expression_fold(binop->right);
            binop->left->parent = binop;
            binop->right->parent = binop;
            return binop;
        }
    }
    else if (unaop) {
        assert(unaop->operand);
        if (unaop->operand->getType() == std::string("Number")) {
            return new Number(calculate_unaop_val(unaop));
        } else {
            ASTNode* newnode = constant_expression_fold(unaop->operand);
            newnode->parent = node;
            unaop->operand = newnode;
            if (newnode->getType() == std::string("Number"))
                return new Number(calculate_unaop_val(unaop));
            else {
                return unaop;
            }
        }
    }
    else {
        unaop->operand = constant_expression_fold(unaop->operand);
        unaop->operand->parent = unaop;
        return unaop;
    }
}

void constant_expression_optimization(ASTNode* root)
{
    NodeStore v = root->getChildren();

    for (NodeStore::iterator it = v.begin(); it != v.end(); ++it)
        if (*it) {
            BinaryOperator* binop = dynamic_cast<BinaryOperator*>(*it);
            if (binop) {
                *it = constant_expression_fold(binop);
                (*it)->parent = root;
            } else {
                UnaryOperator* unaop = dynamic_cast<UnaryOperator*>(*it);
                if (unaop) {
                    *it = constant_expression_fold(unaop);
                    (*it)->parent = root;
                }
            }
            constant_expression_optimization(*it);
        }
    root->setChildren(v);
}


static int compile_function_step(parse_info& pi, function_info& fi, ASTNode *node)
{
    int errors = 0;
    if (node->getType() == std::string("Block")) {
        Block* b = dynamic_cast<Block*>(node);
        for(NodeStore::iterator it = b->list.begin(); it != b->list.end(); ++it)
            if (*it)
                errors += compile_function_step(pi, fi, *it);
    } else if (node->getType() == std::string("List")) {
        List* l = dynamic_cast<List*>(node);
        for(NodeStore::iterator it = l->list.begin(); it != l->list.end(); ++it)
            if (*it)
                errors += compile_function_step(pi, fi, *it);
    } else if (node->getType() == std::string("Declaration")) {
        Declaration *decl = dynamic_cast<Declaration*>(node);
        errors += insert_many_into_symtable(pi, fi.locals, decl->list);
    } else if (node->getType() == std::string("Ident")) {
        Ident* id = dynamic_cast<Ident*>(node);
        if (!find_symbol(pi, id->name)) {
            LOGERROR(id, "unknown identifier \"%s\"\n", id->name.c_str());
            return 1;
        }
    } else if (node->getType() == std::string("CallExpr")) {
        NodeStore v = node->getChildren();
        NodeStore::iterator it = v.begin();
        assert(*it);
        if ((*it)->getType() == std::string("Ident")) {
            Ident* id = dynamic_cast<Ident*>(*it);
            if (id) {
                ASTNode* sym = find_symbol(pi, id->name);
                if (!sym && pi.functions.find(id->name) == pi.functions.end()) {
                    pi.undefinedFuncs.insert(NodeTable::value_type(id->name, id));
                } else if (sym) {
                    ++errors;
                    LOGERROR(id, "symbol \"%s\" is not a function\n", id->name.c_str());
                    LOGLOC(sym, "was defined here\n");
                }
            }
        } else {
            LOGWARNING(*it, "warning: not calling by name\n");
        }
        ++it;
        if (*it)
            errors += compile_function_step(pi, fi, *it);
    } else {
        NodeStore v = node->getChildren();
        for(NodeStore::iterator it = v.begin(); it != v.end(); ++it)
            if (*it)
                errors += compile_function_step(pi, fi, *it);
    }
    return errors;
}

static int compile_function(parse_info& pi, ASTNode* node)
{
    int errors = 0;
    Function* f = dynamic_cast<Function*>(node);
    assert(f);
    FunctionProto *proto = dynamic_cast<FunctionProto*>(f->proto);
    assert(proto);

    function_info fi;
    fi.ast = f;
    fi.index = pi.functions.size();
    fi.name = proto->name;
    pi.locals = &fi.locals;
    errors += fi.readParams(pi, proto->params);
    ASTNode *tmp = find_symbol(pi, fi.name);
    if (tmp) {
        LOGERROR(f, "duplicate declaration of \"%s\"\n", fi.name.c_str());
        LOGLOC(tmp, "previously defined here\n");
        return errors+1;
    }
    pi.undefinedFuncs.erase(fi.name);

    errors += compile_function_step(pi, fi, f->instr);

    pi.locals = 0;
    pi.functions.insert(FunctionTable::value_type(fi.name, fi));
    LOG("%s: read %u params, %u vars\n", fi.name.c_str(), fi.params.size(), fi.locals.size());
    return errors;
}

int check_parse_results(parse_info& pi)
{
    for (NodeTable::iterator it = pi.undefinedFuncs.begin();
            it != pi.undefinedFuncs.end(); ++it) {
        assert(it->second);
        Ident* id = dynamic_cast<Ident*>(it->second);
        if (id) {
            LOGERROR(id, "function called, but undefined: \"%s\"\n", id->name.c_str());
        } else {
            LOGERROR(it->second, "function called, but undefined\n");
        }
        ++pi.errors;
    }
    return (int)pi.undefinedFuncs.size();
}

int validate_tree(parse_info& pi, ASTNode *root)
{
    NodeStore v = root->getChildren();
    for (NodeStore::iterator it = v.begin(); it != v.end(); ++it) {
        if (!(*it))
            continue;
        ASTNode *node = *it;
        std::string type = node->getType();
        if (type == "Break" || type == "Continue")
            pi.errors += !validate_loop_exit(node);
        else if (type == "Declaration") {
            bool validDecl = validate_declarations(node);
            if (validDecl)
                pi.errors += !add_declarations_to_symtable(pi, node);
            else
                ++pi.errors;
        } else if (type == "Jump") {
            pi.errors += !validate_jump(node);
        } else if (type == "Label") {
            pi.errors += !add_label(pi, node);
        }
        validate_tree(pi, node);
        if (type == "Function") {
            pi.errors += compile_function(pi, node);
        }
    }
    return pi.errors;
}
