%{
#include <cstdio>
#include <map>
#include <list>

#include "logging.h"
#include "ast.h"

using namespace std;
extern "C"
{
        extern char *curfile;
        extern int lineno;
        extern double linear_const, angular_const;

        int yyparse(void);
        int yylex(void);  
        int yywrap()
        {
                return 1;
        }
        void yyerror(const char* str)
        {
                fprintf(stderr, "%s:%d: %s\n", curfile, lineno, str);
        }
}

RootBlock *root = new RootBlock();

%}


%token SET_SIGNAL_MASK RAISE_SIGNAL DO_TURN DO_MOVE DO_SPIN
%token ACCELERATE SPEED ALONG AROUND TO SPEED_NOW EXPLODE_TYPE EXPLODE
%token DECL_STATIC_VAR DECL_VAR DECL_PIECE EMIT_SFX EMIT_FROM
%token START_SCRIPT CALL_SCRIPT RANDOM_NUMBER
%token SHADE DONT_SHADE CACHE DONT_CACHE SHOW HIDE
%token COB_GET COB_SET COB_SLEEP
%token WAIT_FOR_MOVE WAIT_FOR_TURN STOP_SPIN
%token ATTACH_UNIT DROP_UNIT
%token LINENO PRAGMA
%token BOOL_AND "&&" BOOL_OR "||"
%token PLAY_SOUND CONTINUE BREAK RETURN GOTO

%token CMP_EQ "==" CMP_GT CMP_LT CMP_GTE ">=" CMP_LTE "<=" CMP_NEQ "!="
%token INCREMENT "++" DECREMENT "--" FOR WHILE IF ELSE DO
%token SHIFT_LEFT "<<" SHIFT_RIGHT ">>"

%token ANGULAR_EXPR_OPEN "<("
%token ANGULAR_EXPR_CLOSE ")>"

%union {
        ASTNode *ast;
        double number;
        char *string;
}

%token <string> IDENTIFIER
%token <number> NUMBER
%token <number> AXIS

%type <ast> expression primary_expression postfix_expression get_expression
%type <ast> rand_expression unary_expression additive_expression
%type <ast> multiplicative_expression shift_expression
%type <ast> relational_expression equality_expression
%type <ast> exclusive_or_expression inclusive_or_expression
%type <ast> and_expression logical_and_expression
%type <ast> logical_or_expression assignment_expression
%type <ast> argument_expression_list conditional_expression
//%type <ast> constant_expression

%type <ast> statement expression_statement jump_statement
%type <ast> selection_statement iteration_statement
%type <ast> set_statement labeled_statement
%type <ast> compound_statement move_statement spin_statement
%type <ast> wait_statement sleep_statement emit_sfx_statement
%type <ast> script_statement misc_statement attach_statement
%type <ast> visibility_statement explode_statement signal_statement
%type <ast> declaration_list declaration
%type <ast> statement_list

%type <ast> type_specifier init_declarator_list init_declarator declarator
%type <ast> initializer identifier_list pragma function_definition
%type <ast> translation_unit external_declaration


%start translation_unit
%error-verbose
%defines
%debug

%%

primary_expression
	: IDENTIFIER            { $$ = new Ident($1); }
	| NUMBER                { $$ = new Number($1); }
	| AXIS                  { $$ = new Axis($1); }
//	| STRING_LITERAL
	| '(' expression ')'    { $$ = $2; }
        | '[' expression ']'    { $$ = new Multiply(new Number(linear_const), $2); }
        //| '<' expression '>'    { $$ = new Multiply(new Number(angular_const), $2); }
        | "<(" expression ")>"    { $$ = new Multiply(new Number(angular_const), $2); }
	;

postfix_expression
	: primary_expression
	| postfix_expression '(' ')'    { $$ = new CallExpr($1, 0); }
	| postfix_expression '(' argument_expression_list ')' { $$ = new CallExpr($1, $3); }
	| postfix_expression INCREMENT  { $$ = new Increment($1); }
	| postfix_expression DECREMENT  { $$ = new Decrement($1); }
	;


argument_expression_list
	: assignment_expression
	| argument_expression_list ',' assignment_expression  { $$ = new Comma($1, $3) }
	;

unary_expression
	: postfix_expression
	| INCREMENT unary_expression    { $$ = new Increment($2); }
	| DECREMENT unary_expression    { $$ = new Decrement($2); }
        | '+' unary_expression       { $$ = $2; }
        | '-' unary_expression       { $$ = new Negation($2); }
        | '!' unary_expression       { $$ = new LogicalNot($2); }
        | '~' unary_expression       { $$ = new BitwiseNot($2); }
        | get_expression
        | rand_expression
	;

get_expression
        : COB_GET primary_expression { $$ = new GetOp($2); }
        | COB_GET primary_expression '(' ')' { $$ = new GetOp($2); }
        | COB_GET primary_expression '(' assignment_expression ')'
                { $$ = new GetOp($2, $4); }
        | COB_GET primary_expression '(' assignment_expression ','
                assignment_expression ')'       { $$ = new GetOp($2, $4, $6); }
        | COB_GET primary_expression '(' assignment_expression ','
                assignment_expression ',' assignment_expression ')'
                                        { $$ = new GetOp($2, $4, $6, $8); }
        | COB_GET primary_expression '(' assignment_expression ','
                assignment_expression ',' assignment_expression ','
                assignment_expression ')' { $$ = new GetOp($2, $4, $6, $8, $10); }
        ;

rand_expression
        : RANDOM_NUMBER '(' assignment_expression ',' assignment_expression ')'
                { $$ = new TAOperation("rand", $3, $5); }
        ;

multiplicative_expression
	: unary_expression
	| multiplicative_expression '*' unary_expression        { $$ = new Multiply($1, $3); }
	| multiplicative_expression '/' unary_expression        { $$ = new Divide($1, $3); }
	| multiplicative_expression '%' unary_expression        { $$ = new Modulo($1, $3); }
	;

additive_expression
	: multiplicative_expression
	| additive_expression '+' multiplicative_expression     { $$ = new Add($1, $3); }
	| additive_expression '-' multiplicative_expression     { $$ = new Subtract($1, $3); }
	;

shift_expression
	: additive_expression
	| shift_expression SHIFT_LEFT additive_expression       { $$ = new ShiftLeft($1, $3); }
	| shift_expression SHIFT_RIGHT additive_expression      { $$ = new ShiftRight($1, $3); }
	;

relational_expression
	: shift_expression
	| relational_expression '<' shift_expression            { $$ = new LessThan($1, $3); }
	| relational_expression '>' shift_expression            { $$ = new GreaterThan($1, $3); }
	| relational_expression CMP_GTE shift_expression        { $$ = new GreaterEqual($1, $3); }
	| relational_expression CMP_LTE shift_expression        { $$ = new LessEqual($1, $3); }
	;

equality_expression
	: relational_expression
	| equality_expression CMP_EQ relational_expression      { $$ = new Equal($1, $3); }
	| equality_expression CMP_NEQ relational_expression     { $$ = new NotEqual($1, $3); }
	;

and_expression
	: equality_expression
	| and_expression '&' equality_expression                { $$ = new BitwiseAnd($1, $3); }
	;

exclusive_or_expression
	: and_expression
	| exclusive_or_expression '^' and_expression            { $$ = new BitwiseXor($1, $3); }
	;

inclusive_or_expression
	: exclusive_or_expression
	| inclusive_or_expression '|' exclusive_or_expression   { $$ = new BitwiseOr($1, $3); }
	;

logical_and_expression
	: inclusive_or_expression
	| logical_and_expression BOOL_AND inclusive_or_expression { $$ = new LogicalOr($1, $3); }
	;

logical_or_expression
	: logical_and_expression
	| logical_or_expression BOOL_OR logical_and_expression  { $$ = new LogicalAnd($1, $3); }
	;


conditional_expression
	: logical_or_expression
	| logical_or_expression '?' expression ':' conditional_expression
                        { $$ = new IfThenElse($1, $3, $5); }
	;

assignment_expression
	: conditional_expression
        // too expressive?
//	| unary_expression '=' assignment_expression
	| IDENTIFIER '=' assignment_expression
                        { $$ = new Assign(new Ident($1), $3); }
	;

/*
assignment_operator
	: '='
	| MUL_ASSIGN
	| DIV_ASSIGN
	| MOD_ASSIGN
	| ADD_ASSIGN
	| SUB_ASSIGN
	| LEFT_ASSIGN
	| RIGHT_ASSIGN
	| AND_ASSIGN
	| XOR_ASSIGN
	| OR_ASSIGN
	;
*/

expression
	: assignment_expression
	| expression ',' assignment_expression  { $$ = new Comma($1, $3); }
	;

/*
constant_expression
	: conditional_expression
	;
*/

declaration
	: type_specifier ';'                            { $$ = $1; }
	| type_specifier init_declarator_list ';'       { $$ = new Declaration($1, $2); }
	;


init_declarator_list
	: init_declarator
	| init_declarator_list ',' init_declarator      { $$ = new Comma($1, $3); }
	;

init_declarator
	: declarator
	| declarator '=' initializer            { $$ = new Assign($1, $3); }
	;

type_specifier
	: DECL_STATIC_VAR       { $$ = new Typename("static-var"); }
	| DECL_VAR              { $$ = new Typename("var"); }
	| DECL_PIECE            { $$ = new Typename("piece"); }
	;

declarator
	: IDENTIFIER            { $$ = new Ident($1) }
	| '(' declarator ')'    { $$ = $2 }
	| IDENTIFIER '(' identifier_list ')'    { $$ = new FunctionProto($1, $3) }
	| IDENTIFIER '(' ')'                    { $$ = new FunctionProto($1, 0) }
	;

identifier_list
	: IDENTIFIER            { $$ = new Ident($1) }
	| identifier_list ',' IDENTIFIER        { $$ = new Comma($1, new Ident($3)) }
	;

initializer
	: assignment_expression
	;

statement
	: labeled_statement
	| compound_statement
	| expression_statement
	| selection_statement
	| iteration_statement
        | declaration_list
	| jump_statement
        | set_statement
        | move_statement
        | spin_statement
        | wait_statement
        | sleep_statement
        | emit_sfx_statement
        | script_statement
        | misc_statement // cache, shade
        | attach_statement
        | visibility_statement
        | explode_statement
        | signal_statement
        /*
        | play_sound_statement
        */
	;


///////////////////////////////////////////////////////////////////////
// spring ops
set_statement
        : COB_SET expression TO expression { $$ = new SetOp($2, $4); }
        ;

move_statement
        : DO_MOVE expression TO primary_expression expression SPEED_NOW
                { $$ = new TAOperation("move-now", $2, $4, $5); }
        | DO_MOVE expression TO primary_expression expression SPEED expression
                { $$ = new TAOperation("move", $2, $4, $5, $7); }
        | DO_MOVE expression TO primary_expression expression SPEED expression ACCELERATE expression
                { $$ = new TAOperation("move", $2, $4, $5, $7, $9); }
        | DO_TURN expression TO primary_expression expression SPEED_NOW
                { $$ = new TAOperation("turn-now", $2, $4, $5); }
        | DO_TURN expression TO primary_expression expression SPEED expression
                { $$ = new TAOperation("turn", $2, $4, $5, $7); }
        | DO_TURN expression TO primary_expression expression SPEED expression ACCELERATE expression
                { $$ = new TAOperation("turn", $2, $4, $5, $7, $9); }
        ;

spin_statement
        : DO_SPIN expression AROUND expression SPEED expression
                { $$ = new TAOperation("spin", $2, $4, $6); }
        | DO_SPIN expression AROUND expression SPEED expression ACCELERATE expression
                { $$ = new TAOperation("spin", $2, $4, $6, $8); }
        | STOP_SPIN expression
                { $$ = new TAOperation("stop-spin", $2); }
        | STOP_SPIN expression AROUND expression
                { $$ = new TAOperation("stop-spin", $2, $4); }
        ;

wait_statement
        : WAIT_FOR_MOVE expression ALONG expression
                { $$ = new TAOperation("wait-for-move", $2, $4); }
        | WAIT_FOR_TURN expression AROUND expression
                { $$ = new TAOperation("wait-for-turn", $2, $4); }
        ;

sleep_statement
        : COB_SLEEP expression
                { $$ = new TAOperation("sleep", $2); }
        ;

emit_sfx_statement
        : EMIT_SFX expression EMIT_FROM expression
                { $$ = new TAOperation("emit-sfx", $2, $4); }
        ;

script_statement
        : CALL_SCRIPT IDENTIFIER '(' argument_expression_list ')'
                { $$ = new Call($2, $4); }
        | CALL_SCRIPT IDENTIFIER '(' ')'
                { $$ = new Call($2); }
        | START_SCRIPT IDENTIFIER '(' argument_expression_list ')'
                { $$ = new StartScript($2, $4); }
        | START_SCRIPT IDENTIFIER '(' ')'
                { $$ = new StartScript($2); }
        ;

// mostly no-ops in spring
misc_statement
        : CACHE expression      { $$ = $2 }
        | DONT_CACHE expression { $$ = $2 }
        | SHADE expression      { $$ = $2 }
        | DONT_SHADE expression { $$ = $2 }
        ;

attach_statement
        : ATTACH_UNIT expression TO expression
                { $$ = new TAOperation("attach-unit", $2, $4); }
        | DROP_UNIT expression
                { $$ = new TAOperation("drop-unit", $2); }
        ;

visibility_statement
        : SHOW expression
                { $$ = new TAOperation("show", $2); }
        | HIDE expression
                { $$ = new TAOperation("hide", $2); }
        ;

signal_statement
        : RAISE_SIGNAL expression
                { $$ = new TAOperation("signal", $2); }
        | SET_SIGNAL_MASK expression
                { $$ = new TAOperation("set-signal-mask", $2); }
        ;

explode_statement
        : EXPLODE expression EXPLODE_TYPE expression
                { $$ = new TAOperation("explode", $2, $4); }
        ;

///////////////////////////////////////////////////////////////////////

labeled_statement
	: IDENTIFIER ':' statement      { $$ = new Label($1, $3); }
        /*
	| CASE constant_expression ':' statement
	| DEFAULT ':' statement
        */
	;

compound_statement
	: '{' '}'                                       { $$ = new ASTNode() }
	| '{' statement_list '}'                        { $$ = $2 }
	;

declaration_list
	: declaration
	| declaration_list declaration  { $$ = new Semicolon($1, $2) }
	;

statement_list
	: statement
	| statement_list statement      { $$ = new Semicolon($1, $2) }
	;

expression_statement
	: ';'                   { $$ = new ASTNode() }
	| expression ';'        { $$ = $1; }
	;

selection_statement
	: IF '(' expression ')' statement       { $$ = new IfThenElse($3, $5); }
	| IF '(' expression ')' statement ELSE statement
                                                { $$ = new IfThenElse($3, $5, $7); }
	/*| SWITCH '(' expression ')' statement*/
	;

        // Loop(body, init, precond, postcond, before, after)
iteration_statement
	: WHILE '(' expression ')' statement
                                { $$ = new Loop($5, 0, $3, 0, 0, 0); }
	| DO statement WHILE '(' expression ')' ';'
                                { $$ = new Loop($2, 0, 0, $5, 0, 0); }
	| FOR '(' expression_statement expression_statement ')' statement
                                { $$ = new Loop($6, $3, $4, 0, 0, 0); }
	| FOR '(' expression_statement expression_statement expression ')' statement
                                { $$ = new Loop($7, $3, $4, 0, 0, $5); }
        // scriptor compatibility
	| FOR '(' expression_statement expression_statement expression_statement ')' statement
                                { $$ = new Loop($7, $3, $4, 0, 0, $5); }
	;

jump_statement
	: GOTO IDENTIFIER ';'   { $$ = new Jump($2); }
	| CONTINUE ';'          { $$ = new Continue(); }
	| BREAK ';'             { $$ = new Break(); }
	| RETURN ';'            { $$ = new Return(0); }
	| RETURN expression ';' { $$ = new Return($2); }
	;

translation_unit
	: external_declaration                  { $$ = $1; $$->parent = root; root->list.push_back($1); }
	| translation_unit external_declaration { $$ = $2; $$->parent = root; root->list.push_back($2); }
        | /* empty */                           {  }
	;

external_declaration
	: function_definition
	| declaration
        | pragma
	;

function_definition
	: declarator compound_statement { $$ = new Function($1, $2) }
	;

pragma
        : PRAGMA IDENTIFIER NUMBER      { $$ = new Pragma($2, $3); }
        | PRAGMA IDENTIFIER IDENTIFIER  { $$ = new Pragma($2, $3); }
        | PRAGMA IDENTIFIER             { $$ = new Pragma($2, 0); }
        ;
%%
