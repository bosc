#ifndef LOGGING_H
#define LOGGING_H


#ifdef __cplusplus

#include <cstdio>

#define LOGERROR(ast, ...)  do { std::fprintf(stderr, "%s:  error: ", (ast)->locStr().c_str()); \
                                 std::fprintf(stderr, __VA_ARGS__); } while(0)
#define LOGWARNING(ast, ...)  do { std::fprintf(stderr, "%s:warning: ", (ast)->locStr().c_str()); \
                                 std::fprintf(stderr, __VA_ARGS__); } while(0)
#define LOGLOC(ast, ...)  do { std::fprintf(stderr, "%s:         ", (ast)->locStr().c_str()); \
                                 std::fprintf(stderr, __VA_ARGS__); } while(0)
#define EPRINTF(...) std::fprintf(stderr, __VA_ARGS__)
#ifndef NOLOGGING
#define LOG(...)  (std::fprintf(stderr, __VA_ARGS__))
#else
#define LOG(...)  (0)
#endif

#else // __cplusplus

#include <stdio.h>

#define EPRINTF(...) fprintf(stderr, __VA_ARGS__)
#ifndef NOLOGGING
#define LOG(...)  (fprintf(stderr, __VA_ARGS__))
#else
#define LOG(...)  (0)
#endif

#endif // __cplusplus



#endif
