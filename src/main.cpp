#include <cstdio>
#include <cstdlib>

#include <stdexcept>

#include "ast.h"
#include "logging.h"
#include "compiler.h"
#include "codegen_basm.h"


using namespace std;

extern "C"
{
    int yyparse(void);
    int yylex(void); 
    int yyparse(void);
    int yyerror(const char*);
    extern FILE* yyin;
}

extern Block *root;

static char _filename_to_del[PATH_MAX];
void kill_at_exit(void)
{
    unlink(_filename_to_del);
}

/// Preprocess a file and return a pointer to the preprocessed data
FILE* preprocess_file(const char* filename)
{
    char tmp[L_tmpnam+1];
    char *ptr;
    char cmdline[L_tmpnam+strlen(filename)+16];
    char tmp2[PATH_MAX];

    ptr = tmpnam(tmp);
    if (!ptr)
        throw runtime_error("unable to allocate temporary file");
    if (getenv("TEMP")) {
        strcpy(tmp2, getenv("TEMP"));
        strcat(tmp2, ptr);
        ptr = tmp2;
    }
    sprintf(cmdline, "cpp \"%s\" -o \"%s\"", filename, ptr);
    LOG("running cpp: %s\n", cmdline);
    if (system(cmdline) != 0)
        throw runtime_error("unable to call cpp");
    strcpy(_filename_to_del, ptr);
    atexit(kill_at_exit);
    return fopen(ptr, "r");
}

int main(int argc, char** argv)
{
    try {
        if (argc > 2)
            throw runtime_error("bad command line, usage: bosc <filename>");
        else if (argc == 2) {
            if (strcmp("--", argv[1]) == 0)
                yyin = stdin;
            else {
                FILE* test = fopen(argv[1], "r");
                if (test) fclose(test);
                else throw runtime_error("cannot open input file");
                yyin = preprocess_file(argv[1]);
                if (!yyin)
                    throw runtime_error("cannot run cpp");
            }
        }
        else
            yyin = stdin;
    } catch(const runtime_error& e) {
        fprintf(stderr, "runtime error: %s\n", e.what());
        fclose(yyin);
        return 1;
    }
    int retval = yyparse();
    fclose(yyin);
    simplify_ast(root);
    constant_expression_optimization(root);
    if (!tree_check_parents(root)) {
        EPRINTF("internal error: AST check failed\n");
        printf("%s", root->toString().c_str());
        return 255;
    }
    parse_info pi;
    validate_tree(pi, root);
    check_parse_results(pi);
    if (pi.errors != 0) {
        EPRINTF("%d error(s), compilation aborted\n", pi.errors);
        return 1;
    }
    printf("%s", root->toString().c_str());

    CodeGeneratorBasm codegen(pi);
    codegen.init();
    codegen.emitAST(root);
    printf("<!--\n%s\n-->", codegen.getCode().c_str());
    return retval;
}
