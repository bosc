#ifndef AST_H
#define AST_H

#include <cassert>

#include <string>
#include <sstream>
#include <vector>

class ASTNode;

#include "parser.tab.hpp"
#include "logging.h"

// utility stuff

typedef std::vector<ASTNode*> NodeStore;

static inline std::string numtostr(double num)
{
    std::stringstream s;
    s.precision(12);
    s << num;
    return s.str();
}

extern "C" {
    extern int lineno;
    extern char* curfile;
}

/** base class. also works as a noop node. */
class ASTNode
{
protected:
    void setParent(ASTNode* p) { if (p) p->parent = this; }
public:
    struct {
        std::string file;
        int line;
    } loc;
    ASTNode *parent;
    ASTNode() : parent(0) { if (curfile) loc.file = curfile; loc.line = lineno; }
    ASTNode(ASTNode *p) : parent(p) { if (curfile) loc.file = curfile; loc.line = lineno; }
    virtual ~ASTNode() {};

    std::string locStr() const
    {
        std::ostringstream s;
        s << loc.file << ":" << loc.line;
        std::string str = s.str();
#ifdef BROKEN_FLEX
        for (unsigned i = str.find ("\\\\"); i != std::string::npos; i = str.find ("\\\\", i + 1))
            str.replace(i, 2, "\\");
        for (unsigned i = str.find ("\\/"); i != std::string::npos; i = str.find ("\\/", i + 1))
            str.replace(i, 2, "/");
#endif
        return str;
    }
    std::string locXML() const
    { return " loc=\"" + locStr() + "\" "; }
    virtual const char *getType() const { return "ASTNode"; }
    virtual std::string toString() const { return std::string("<BasicAstNode" + locXML() + "/>"); }
    virtual NodeStore getChildren() const { return NodeStore(); }
    virtual void setChildren(const NodeStore &v) { }
};

class Block : public ASTNode {
public:
    NodeStore list;
    Block() {};
    const char *getType() const { return "Block"; }
    std::string toString() const
    {
        std::string arg;
        for (NodeStore::const_iterator it = list.begin(); it != list.end(); ++it)
            arg += (*it)->toString();
        return "<block>" + arg + "</block>";
    }
    NodeStore getChildren() const { return list; }
    void setChildren(const NodeStore& v) { list = v; }
};

class RootBlock : public Block {
public:
    const char *getType() const { return "RootBlock"; }
    std::string toString() const
    {
        std::string arg;
        for (NodeStore::const_iterator it = list.begin(); it != list.end(); ++it)
            arg += (*it)->toString();
        return "<root>" + arg + "</root>";
    }
};

class List : public ASTNode {
public:
    NodeStore list;
    List() {};
    const char *getType() const { return "List"; }
    std::string toString() const
    {
        std::string arg;
        for (NodeStore::const_iterator it = list.begin(); it != list.end(); ++it)
            arg += (*it)->toString();
        return "<list>" + arg + "</list>";
    }
    NodeStore getChildren() const { return list; }
    void setChildren(const NodeStore& v) { list = v; }
};

//////////////////////////////////////////////////////////////////////
// leafs
class Ident : public ASTNode
{
public:
    std::string name;
    Ident(std::string ident)
        : name(ident) {}

    const char *getType() const { return "Ident"; }
    std::string toString() const { return "<ident name=\""+name+"\"/>"; }
};


class Number : public ASTNode
{
public:
    double val;
    Number(double n): val(n) {}
    const char *getType() const { return "Number"; }
    std::string toString() const { return std::string("<number val=\"")+numtostr(val)+"\"/>"; }
};

//////////////////////////////////////////////////////////////////////
// nodes


//
// unary ops
//

class UnaryOperator : public ASTNode {
public:
    std::string oper;
    ASTNode *operand;
    UnaryOperator(std::string op, ASTNode *e)
        : oper(op), operand(e)
    { setParent(operand); }

    const char *getType() const { return ("UnaryOperator<" + oper + ">").c_str(); }
    virtual std::string getName() const { return "unaryOp"; }
    std::string toString() const
    { return "<" + getName() + " op=\"" + oper + "\" " + locXML() + ">"
                + operand->toString()
                + "</" + getName() + ">"; }

    NodeStore getChildren() const
    { NodeStore v; v.push_back(operand); return v; }
    void setChildren(const NodeStore& v)
    { assert(v.size() == 1); operand = v[0]; }
};

class Increment : public UnaryOperator
{
public:
    Increment(ASTNode *node): UnaryOperator("++", node) {}
    const char *getType() const { return "Increment"; }
    std::string getName() const { return "incr"; }
};

class Decrement : public UnaryOperator {
public:
    Decrement(ASTNode *node): UnaryOperator("--", node) {}
    const char *getType() const { return "Decrement"; }
    std::string getName() const { return "decr"; }
};

class Negation : public UnaryOperator {
public:
    Negation(ASTNode *e)
        : UnaryOperator("-", e)
    {}
    const char *getType() const { return "Negation"; }
    std::string getName() const { return "neg"; }
};

class LogicalNot : public UnaryOperator {
public:
    LogicalNot(ASTNode *e)
        : UnaryOperator("!", e)
    {}
    const char *getType() const { return "LogicalNot"; }
    std::string getName() const { return "boolnot"; }
};

class BitwiseNot : public UnaryOperator {
public:
    BitwiseNot(ASTNode *e)
        : UnaryOperator("~", e)
    {}
    const char *getType() const { return "BitwiseNot"; }
    std::string getName() const { return "bitnot"; }
};

//
// binary ops
//

class BinaryOperator : public ASTNode {
public:
    ASTNode *left, *right;
    std::string oper;
    BinaryOperator(std::string op, ASTNode *l, ASTNode *r)
        : left(l), right(r), oper(op)
    { setParent(left); setParent(right); }

    virtual std::string getName() const { return "binaryOp"; }
    const char *getType() const { return ("BinaryOperator<"+oper+">").c_str(); }
    std::string toString() const
    { return "<" + getName() + " op=\"" + oper + "\"" + locXML() + "><left>"
            + left->toString() + "</left><right>"
            + right->toString() + "</right></" + getName() + ">"; }

    NodeStore getChildren() const
    { NodeStore v; v.push_back(left); v.push_back(right); return v; }
    void setChildren(const NodeStore& v)
    { assert(v.size() == 2); left = v[0]; right = v[1]; }

    virtual BinaryOperator* invert() { return this; }
};

// math

class Add : public BinaryOperator {
public:
    Add(ASTNode *l, ASTNode *r)
        : BinaryOperator("+", l, r) {}
    const char *getType() const { return "Add"; }
    std::string getName() const { return "add"; }
};

class Subtract : public BinaryOperator {
public:
    Subtract(ASTNode *l, ASTNode *r)
        : BinaryOperator("-", l, r) {}
    const char *getType() const { return "Subtract"; }
    std::string getName() const { return "sub"; }

    Add* invert()
    {
        Number* n = dynamic_cast<Number*>(right);
        if (n)
            return new Add(left, new Number(-n->val));
        n = dynamic_cast<Number*>(left);
        if (left)
            return new Add(new Number(-n->val), right);
        return new Add(left, new Negation(right));
    }
};

class Multiply : public BinaryOperator {
public:
    Multiply(ASTNode *l, ASTNode *r)
        : BinaryOperator("*", l, r) {}
    const char *getType() const { return "Multiply"; }
    std::string getName() const { return "mul"; }
};

class Divide : public BinaryOperator {
public:
    Divide(ASTNode *l, ASTNode *r)
        : BinaryOperator("/", l, r) {}
    const char *getType() const { return "Divide"; }
    std::string getName() const { return "div"; }

    Multiply* invert()
    {
        Number* n = dynamic_cast<Number*>(right);
        if (n)
            return new Multiply(left, new Number(1./n->val));
        n = dynamic_cast<Number*>(left);
        if (left)
            return new Multiply(new Number(1./n->val), right);
        return new Multiply(left, new Divide(new Number(1.), right));
    }
};

class Modulo : public BinaryOperator {
public:
    Modulo(ASTNode *l, ASTNode *r)
        : BinaryOperator("%", l, r) {}
    const char *getType() const { return "Modulo"; }
    std::string getName() const { return "mod"; }
};


// assignment

class Assign : public BinaryOperator {
public:
    Assign(ASTNode *l, ASTNode *r)
        : BinaryOperator("=", l, r) {}
    const char *getType() const { return "Assign"; }
    std::string getName() const { return "assign"; }
};

// relational

class LessThan : public BinaryOperator {
public:
    LessThan(ASTNode *l, ASTNode *r)
        : BinaryOperator("&lt;", l, r) {}
    const char *getType() const { return "LessThan"; }
    std::string getName() const { return "lt"; }
};

class GreaterThan : public BinaryOperator {
public:
    GreaterThan(ASTNode *l, ASTNode *r)
        : BinaryOperator("&gt;", l, r) {}
    const char *getType() const { return "GreaterThan"; }
    std::string getName() const { return "gt"; }
};

class LessEqual : public BinaryOperator {
public:
    LessEqual(ASTNode *l, ASTNode *r)
        : BinaryOperator("&lt;=", l, r) {}
    const char *getType() const { return "LessEqual"; }
    std::string getName() const { return "lteq"; }
};

class GreaterEqual : public BinaryOperator {
public:
    GreaterEqual(ASTNode *l, ASTNode *r)
        : BinaryOperator("&gt;=", l, r) {}
    const char *getType() const { return "GreaterEqual"; }
    std::string getName() const { return "gteq"; }
};

class Equal : public BinaryOperator {
public:
    Equal(ASTNode *l, ASTNode *r)
        : BinaryOperator("==", l, r) {}
    const char *getType() const { return "Equal"; }
    std::string getName() const { return "eq"; }
};

class NotEqual : public BinaryOperator {
public:
    NotEqual(ASTNode *l, ASTNode *r)
        : BinaryOperator("!=", l, r) {}
    const char *getType() const { return "NotEqual"; }
    std::string getName() const { return "ne"; }
};

// bitwise

class BitwiseAnd : public BinaryOperator {
public:
    BitwiseAnd(ASTNode *l, ASTNode *r)
        : BinaryOperator("&amp;", l, r) {}
    const char *getType() const { return "BitwiseAnd"; }
    std::string getName() const { return "bitand"; }
};

class BitwiseOr : public BinaryOperator {
public:
    BitwiseOr(ASTNode *l, ASTNode *r)
        : BinaryOperator("|", l, r) {}
    const char *getType() const { return "BitwiseOr"; }
    std::string getName() const { return "bitor"; }
};

class BitwiseXor : public BinaryOperator {
public:
    BitwiseXor(ASTNode *l, ASTNode *r)
        : BinaryOperator("^", l, r) {}
    const char *getType() const { return "BitwiseXor"; }
    std::string getName() const { return "bitxor"; }
};

class ShiftLeft : public BinaryOperator {
public:
    ShiftLeft(ASTNode *l, ASTNode *r)
        : BinaryOperator("&lt;&lt;", l, r) {}
    const char *getType() const { return "ShiftLeft"; }
    std::string getName() const { return "shl"; }
};

class ShiftRight : public BinaryOperator {
public:
    ShiftRight(ASTNode *l, ASTNode *r)
        : BinaryOperator("&gt;&gt;", l, r) {}
    const char *getType() const { return "ShiftRight"; }
    std::string getName() const { return "shr"; }
};

// logical

class LogicalAnd : public BinaryOperator {
public:
    LogicalAnd(ASTNode *l, ASTNode *r)
        : BinaryOperator("&amp;&amp;", l, r) {}
    const char *getType() const { return "LogicalAnd"; }
    std::string getName() const { return "booland"; }
};

class LogicalOr : public BinaryOperator {
public:
    LogicalOr(ASTNode *l, ASTNode *r)
        : BinaryOperator("||", l, r) {}
    const char *getType() const { return "LogicalOr"; }
    std::string getName() const { return "boolor"; }
};

// glue ops

class GlueOperator : public BinaryOperator {
public:
    void traverseTree(NodeStore &c, ASTNode* next);
    GlueOperator(std::string n, ASTNode *l, ASTNode *r)
        : BinaryOperator(n, l, r)
    {}
    const char *getType() const { return "GlueOperator"; }
    std::string getName() const { return "glue"; }
};

class Comma : public GlueOperator {
public:
    Comma(ASTNode *l, ASTNode *r)
        : GlueOperator(",", l, r) {}
    const char *getType() const { return "Comma"; }
    std::string getName() const { return "comma"; }
};

class Semicolon : public GlueOperator {
public:
    Semicolon(ASTNode *l, ASTNode *r)
        : GlueOperator(";", l, r) {}
    const char *getType() const { return "Semicolon"; }
    std::string getName() const { return "semicolon"; }
};

//
// flow control
//

class IfThenElse : public ASTNode
{
public:
    ASTNode *cond, *trueexp, *falseexp;
    IfThenElse(ASTNode *c, ASTNode *t, ASTNode *f = 0)
        : cond(c), trueexp(t), falseexp(f)
    {
        setParent(cond);
        setParent(trueexp);
        setParent(falseexp);
    }
    const char *getType() const { return "IfThenElse"; }
    std::string toString() const
    { return "<cond>"
                  "<if" + locXML() + ">" + cond->toString() + "</if>"
                + "<then>" + trueexp->toString() + "</then>"
                + ( falseexp ? "<else>" + falseexp->toString() + "</else>" : "")
                + "</cond>"; }

    NodeStore getChildren() const
    { NodeStore v; v.push_back(cond); v.push_back(trueexp);
        v.push_back(falseexp); return v; }
    void setChildren(const NodeStore& v)
    { assert(v.size() == 3); cond = v[0]; trueexp = v[1]; falseexp = v[2]; }
};


/*** Loop node.
 *
 * pseudocode:
 *
 *      init
 * loop:
 *      if not precond:
 *              goto end
 *      before
 *      body
 *      after
 *      if not postcond:
 *              goto end
 *      goto loop
 * end:
 **/

class Loop : public ASTNode
{
public:
    /// loop initialization
    ASTNode *init;
    /// loop precondition
    ASTNode *precond;
    /// loop postcondition
    ASTNode *postcond;
    /// execute before any statements
    ASTNode *before;
    /// execute after any statements
    ASTNode *after;
    /// loop body
    ASTNode *body;

    Loop(ASTNode *b, ASTNode *i = 0, ASTNode *pre = 0, ASTNode *post = 0,
            ASTNode *bef = 0, ASTNode *aft = 0)
        : init(i), precond(pre), postcond(post), before(bef), after(aft), body(b)
    {
        setParent(body);
        setParent(init);
        setParent(precond);
        setParent(postcond);
        setParent(before);
        setParent(precond);
        setParent(after);
    }

    const char *getType() const { return "Loop"; }
    std::string toString() const
    { return "<loop" + locXML() + ">" 
                + ( init ? "<init>" + init->toString() + "</init>" : "")
                + ( precond ? "<precond>" + precond->toString() + "</precond>" : "")
                + ( postcond ? "<postcond>" + postcond->toString() + "</postcond>" : "")
                + ( before ? "<before>" + before->toString() + "</before>" : "")
                + ( after ? "<after>" + after->toString() + "</after>" : "")
                + "<body>" + body->toString() + "</body>"
                + "</loop>"; }

    NodeStore getChildren() const
    {
        NodeStore v;
        v.reserve(6);
        v.push_back(init);
        v.push_back(precond);
        v.push_back(postcond);
        v.push_back(before);
        v.push_back(after);
        v.push_back(body);
        return v;
    }
    void setChildren(const NodeStore& v)
    {
        assert(v.size() == 6);
        NodeStore::const_iterator it = v.begin();
        init = *it++;
        precond = *it++;
        postcond = *it++;
        before = *it++;
        after = *it++;
        body = *it++;
    }
};

class BlockControl : public ASTNode {
public:
    BlockControl() {}
    
    ASTNode* findParent(std::string type)
    {
        if (!parent) return 0;
        ASTNode *cur = parent;
        while (cur && cur->getType() != type)
            cur = cur->parent;
        return cur;
    }

    const char *getType() const { return "BlockControl"; }
    std::string toString() const { return "<blockcontrol" + locXML() + "/>"; }
};

class LoopControl : public BlockControl {
public:
    LoopControl() {}
    ASTNode* findParent() { return BlockControl::findParent("Loop"); }
    const char *getType() const { return "LoopControl"; }
    std::string toString() const { return "<loopcontrol" + locXML() + "/>"; }
};

class Continue : public LoopControl {
public:
    Continue() {}
    const char *getType() const { return "Continue"; }
    std::string toString() const { return "<continue" + locXML() + "/>"; }
};

class Break : public LoopControl {
public:
    Break() {}
    const char *getType() const { return "Break"; }
    std::string toString() const { return "<break" + locXML() + "/>"; }
};

class Return : public BlockControl {
public:
    ASTNode *retval;
    Return(ASTNode *r) : retval(r) { setParent(r);}
    ASTNode *findParent() { return BlockControl::findParent("Function"); }

    const char *getType() const { return "Return"; }
    std::string toString() const
    { if (!retval) return "<return" + locXML() + "/>";
      else return "<return" + locXML() + ">" + retval->toString() + "</return>"; }

    NodeStore getChildren() const
    { NodeStore v; v.push_back(retval); return v; }
    void setChildren(const NodeStore& v)
    { assert(v.size() == 1); retval = v[0]; }
};

class Label : public ASTNode {
public:
    std::string name;
    ASTNode *rest;
    Label(std::string n, ASTNode *r): name(n), rest(r) { setParent(r); }

    const char *getType() const { return "Label"; }
    std::string toString() const
    { return "<label name=\"" + name + "\"" + locXML() + ">" + rest->toString() + "</label>"; }

    NodeStore getChildren() const
    { NodeStore v; v.push_back(rest); return v; }
    void setChildren(const NodeStore& v)
    { assert(v.size() == 1); rest = v[0]; }
};

class Jump : public ASTNode {
public:
    std::string dest;
    Jump(const char *d): dest(d) {}
    const char *getType() const { return "Jump"; }
    std::string toString() const { return "<jump dest=\"" + dest + "\"" + locXML() + "/>"; }
};

class CallExpr : public ASTNode {
protected:
    void extractArgs(Comma *args)
    {
    }
public:
    ASTNode *call, *args;
    CallExpr(ASTNode *c, ASTNode *a): call(c), args(a)
    {   setParent(c);
        setParent(a); }

    const char *getType() const { return "CallExpr"; }
    std::string toString() const
    {
        if (args) {
            return "<callexpr" + locXML() + "><callee>" + call->toString() + "</callee><args>" + args->toString() + "</args></callexpr>";
        } else {
            return "<callexpr" + locXML() + "><callee>" + call->toString() + "</callee></callexpr>";
        }
    }

    NodeStore getChildren() const
    { NodeStore v; v.push_back(call); v.push_back(args); return v; }
    void setChildren(const NodeStore& v)
    { assert(v.size() == 2); call = v[0]; args = v[1]; }
};

class Call : public ASTNode {
public:
    ASTNode *argv;
    std::string name;
    Call(std::string n): argv(0), name(n) {}
    Call(std::string n, ASTNode *arg): argv(arg), name(n) { setParent(arg); }

    const char *getType() const { return "Call"; }
    std::string toString() const
    {
        if (argv) {
            return "<call name=\"" + name + "\"" + locXML() + ">" + argv->toString() + "</call>";
        } else {
            return "<call name=\"" + name + "\"" + locXML() + "/>";
        }
    }

    // FIXME
    NodeStore getChildren() const
    { NodeStore v; v.push_back(argv); return v; }
    void setChildren(const NodeStore& v)
    { assert(v.size() == 1); argv = v[0]; }
};

class StartScript : public Call {
public:
    StartScript(std::string n): Call(n) {}
    StartScript(std::string n, ASTNode *arg): Call(n, arg) {}

    const char *getType() const { return "StartScript"; }
    std::string toString() const
    {
        if (argv) {
            return "<startscript name=\"" + name + "\"" + locXML() + ">" + argv->toString() + "</startscript>";
        } else {
            return "<startscript name=\"" + name + "\"" + locXML() + "/>";
        }
    }

};


class FunctionProto : public ASTNode {
public:
    std::string name;
    ASTNode *params;
    FunctionProto(std::string n, ASTNode *p): name(n), params(p)
    { setParent(p); }

    const char *getType() const { return "FunctionProto"; }
    std::string toString() const
    {
        if (params) return "<functionproto name=\"" + name + "\"" + locXML() + ">"
                + params->toString() + "</functionproto>";
        else return "<functionproto name=\"" + name + "\"" + locXML() + "/>";
    }

    NodeStore getChildren() const
    { NodeStore v; v.push_back(params); return v; }
    void setChildren(const NodeStore& v)
    { assert(v.size() == 1); params = v[0]; }
};


//
// TA ops
//

class GetOp : public ASTNode {
public:
    ASTNode *val;
    ASTNode *args[4];
    GetOp(ASTNode *v, ASTNode *a=0, ASTNode *b=0, ASTNode* c=0, ASTNode*d = 0)
        : val(v)
    { args[0] = a; args[1] = b; args[2] = c; args[3] = d;
      setParent(a); setParent(b); setParent(c); setParent(d); setParent(v); }
    const char *getType() const { return (args[0] ? "GetNoArgs" : "Get"); }
    std::string toString() const
    { return "<get" + locXML() + "><val>" + val->toString() + "</val>"
                + (args[0] ? "<a1>" + args[0]->toString() + "</a1>" : "")
                + (args[1] ? "<a2>" + args[1]->toString() + "</a2>" : "")
                + (args[2] ? "<a3>" + args[2]->toString() + "</a3>" : "")
                + (args[3] ? "<a4>" + args[3]->toString() + "</a4>" : "")
                + "</get>"; }

    NodeStore getChildren() const
    {
        NodeStore v;
        v.reserve(4);
        for (int i = 0; i<4; ++i)
            v.push_back(args[i]);
        return v;
    }
    void setChildren(const NodeStore& v)
    {
        assert(v.size() == 4);
        NodeStore::const_iterator it = v.begin();
        for (int i = 0; i<4; ++i, ++it)
            args[i] = *it;
    }
};

class SetOp : public ASTNode {
public:
    ASTNode *key, *val;
    SetOp(ASTNode *k, ASTNode *v) : key(k), val(v)
    { setParent(k); setParent(v); }

    const char *getType() const { return "Get"; }
    std::string toString() const
    { return "<set" + locXML() + "><key>" + key->toString() + "</key>"
                "<value>" + val->toString() + "</value></set>"; }

    NodeStore getChildren() const
    { NodeStore v; v.push_back(key); v.push_back(val); return v; }
    void setChildren(const NodeStore& v)
    { assert(v.size() == 2); key = v[0]; val = v[1]; }
};


class Axis : public ASTNode {
public:
    int axis;
    Axis(int a): axis(a) {}
    const char *getType() const { return "Axis"; }
    std::string toString() const { char tmp[] = "x-axis"; tmp[0] += axis; return std::string("<axis name=\"") + std::string(tmp) + std::string("\"" + locXML() + "/>"); }
};

class TAOperation : public ASTNode {
public:
    std::string name;
    ASTNode *args[5];
    TAOperation(std::string n, ASTNode *arg1, ASTNode *arg2 = 0,
            ASTNode *arg3 = 0, ASTNode *arg4 = 0, ASTNode *arg5 = 0)
        : name(n)
    { args[0] = arg1; args[1] = arg2; args[2] = arg3; args[3] = arg4; args[4] = arg5;
      setParent(arg1); setParent(arg2); setParent(arg3); setParent(arg4); setParent(arg5); }
    const char *getType() const { return "TAOperation"; }
    std::string toString() const
    { return "<operation name=\"" + name + "\"" + locXML() + ">"
                "<a1>" + args[0]->toString() + "</a1>"
                + (args[1] ? "<a2>" + args[1]->toString() + "</a2>" : "")
                + (args[2] ? "<a3>" + args[2]->toString() + "</a3>" : "")
                + (args[3] ? "<a4>" + args[3]->toString() + "</a4>" : "")
                + (args[4] ? "<a5>" + args[4]->toString() + "</a5>" : "")
                + "</operation>"; }

    NodeStore getChildren() const
    {
        NodeStore v;
        v.reserve(5);
        for (int i = 0; i<5; ++i)
            v.push_back(args[i]);
        return v;
    }
    void setChildren(const NodeStore& v)
    {
        assert(v.size() == 5);
        NodeStore::const_iterator it = v.begin();
        for (int i = 0; i<5; ++i, ++it)
            args[i] = *it;
    }
};


class Typename : public ASTNode {
public:
    std::string name;
    Typename(std::string n): name(n) {}

    const char *getType() const { return "Typename"; }
    std::string toString() const
    { return "<typename name=\"" + name + "\"" + locXML() + "/>"; }
};

class Declaration : public ASTNode {
public:
    ASTNode *type;
    ASTNode *list;
    Declaration(ASTNode *t, ASTNode *l): type(t), list(l)
    { setParent(t); setParent(l); }

    const char *getType() const { return "Declaration"; }
    std::string toString() const
    { return "<declaration" + locXML() + "><type>" + type->toString() + "</type>"
                "<decllist>" + list->toString() + "</decllist>"
                "</declaration>"; }

    NodeStore getChildren() const
    { NodeStore v; v.push_back(type); v.push_back(list); return v; }
    void setChildren(const NodeStore& v)
    { assert(v.size() == 2); type = v[0]; list = v[1]; }
};


class Pragma : public ASTNode {
public:
    std::string name;
    std::string value;
    double number;
    Pragma(std::string n, double d): name(n), number(d) {}
    Pragma(std::string n, std::string v): name(n), value(v), number(0) {}
    const char *getType() const { return "Pragma"; }
    std::string toString() const { return "<pragma name=\"" + name + "\" value=\"" + value + "\" num=\"" + numtostr(number) + "\"" + locXML() + "/>"; }
};



class Function : public ASTNode {
public:
    ASTNode *proto;
    ASTNode *instr;
    Function(ASTNode *fp, ASTNode *b): proto(fp), instr(b)
    { setParent(fp); setParent(b); }

    const char *getType() const { return "Function"; }
    std::string toString() const
    { return "<function" + locXML() + ">" + proto->toString() + instr->toString() + "</function>"; }

    NodeStore getChildren() const
    { NodeStore v; v.push_back(proto); v.push_back(instr); return v; }
    void setChildren(const NodeStore& v)
    { assert(v.size() == 2); proto = v[0]; instr = v[1]; }

    std::string getFunctionName()
    { FunctionProto *fp = dynamic_cast<FunctionProto*>(proto); if (fp) return fp->name; else return ""; }
};


#endif
