%{
#include <stdio.h>
#include <string.h>
#include <ctype.h>

#include "logging.h"
// hack so compiler doesn't complain
#define ASTNode void
#include "parser.tab.hpp"

int lineno = 1;
char *curfile = NULL;

double angular_const = 182;
double linear_const = 163840;

%}

number          ([0-9]+\.?[0-9]*)|([0-9]*\.[0-9]+)

%%
^#[ \t0-9]+\"[a-zA-Z0-9.\-+<>/\\*$#@]+\"[ \t0-9]*$ {       /* GNU cpp line info */
                          char tmp[strlen(yytext)];
                          sscanf(yytext, "# %d %s", &lineno, tmp);
                          --lineno;
                          free(curfile);
                          curfile = strdup(tmp+1);
                          curfile[strlen(curfile)-1] = 0;
                        }
^"#pragma linear"[ \t]+[0-9]+ { sscanf(yytext, "#pragma linear %lf", &linear_const); }
^"#pragma angular"[ \t]+[0-9]+ { sscanf(yytext, "#pragma angular %lf", &angular_const); }
^"#pragma"              { return PRAGMA; }
^#.+                    { LOG("unknown directive"); }
"set-signal-mask"       { return SET_SIGNAL_MASK; }
"signal"                { return RAISE_SIGNAL; }
"turn"                  { return DO_TURN; }
"move"                  { return DO_MOVE; }
"spin"                  { return DO_SPIN; }
"accelerate"            { return ACCELERATE; }
"speed"                 { return SPEED; }
"now"                   { return SPEED_NOW; }
"along"                 { return ALONG; }
"around"                { return AROUND; }
"to"                    { return TO; }
"static-var"            { return DECL_STATIC_VAR; }
"var"                   { return DECL_VAR; }
"piece"                 { return DECL_PIECE; }
[xyz]"-axis"            { yylval.number = tolower(*yytext) - 'x'; return AXIS; }
"start-script"          { return START_SCRIPT; }
"call-script"           { return CALL_SCRIPT; }
"explode"               { return EXPLODE; }
"type"                  { return EXPLODE_TYPE; }
"emit-sfx"              { return EMIT_SFX; }
"from"                  { return EMIT_FROM; }
"get"                   { return COB_GET; }
"set"                   { return COB_SET; }
"sleep"                 { return COB_SLEEP; }
"wait-for-move"         { return WAIT_FOR_MOVE; }
"wait-for-turn"         { return WAIT_FOR_TURN; }
"stop-spin"             { return STOP_SPIN; }
"attach-unit"           { return ATTACH_UNIT; }
"drop-unit"             { return ATTACH_UNIT; }
"for"                   { return FOR; }
"while"                 { return WHILE; }
"if"                    { return IF; }
"else"                  { return ELSE; }
"do"                    { return DO; }
"rand"                  { return RANDOM_NUMBER; }
"show"                  { return SHOW; }
"hide"                  { return HIDE; }
"cache"                 { return CACHE; }
"dont-cache"            { return DONT_CACHE; }
"shade"                 { return SHADE; }
"dont-shade"            { return DONT_SHADE; }
"play-sound"            { return PLAY_SOUND; }
"true"                  { yylval.number = 1; return NUMBER; }
"false"                 { yylval.number = 0; return NUMBER; }
"return"                { return RETURN; }
"continue"              { return CONTINUE; }
"break"                 { return BREAK; }
"goto"                  { return GOTO; }
"not"                   { return '!'; }
"and"                   { return BOOL_AND; }
"or"                    { return BOOL_AND; }

[a-zA-Z_][a-zA-Z0-9_]*  { yylval.string = strdup(yytext);
                          return IDENTIFIER; }
{number}                { sscanf(yytext, "%lf", &yylval.number);
                          return NUMBER; }
\<[ \t]*-?[ \t]*{number}+[ \t]*\> {
                          sscanf(yytext+1, "%lf", &yylval.number);
                          yylval.number *= angular_const;
                          return NUMBER;
                        }
\[[ \t]*-?[ \t]*{number}+[ \t]*\] {
                          sscanf(yytext+1, "%lf", &yylval.number);
                          yylval.number *= linear_const;
                          return NUMBER;
                        }
                          
"<("                    { return ANGULAR_EXPR_OPEN; }
")>"                    { return ANGULAR_EXPR_CLOSE; }
\{                      { return *yytext; }
\}                      { return *yytext; }
\(                      { return *yytext; }
\)                      { return *yytext; }
;                       { return *yytext; }
,                       { return *yytext; }
"++"                    { return INCREMENT; }
"--"                    { return DECREMENT; }
"&&"                    { return BOOL_AND; }
"||"                    { return BOOL_OR; }
[?|+&\-%*/\^!~]         { return *yytext; }
[\[\]]                  { return *yytext; }
"=="                    { return CMP_EQ; }
\>                      { return *yytext; }
\<                      { return *yytext; }
">="                    { return CMP_GTE; }
"<="                    { return CMP_LTE; }
"!="                    { return CMP_NEQ; }
"<<"                    { return SHIFT_LEFT; }
">>"                    { return SHIFT_RIGHT; }
"="                     { return *yytext; }
":"                     { return *yytext; }
\n                      { ++lineno; }
[ \t]+                  /* ignore whitespace */;
.                       { EPRINTF("%s:%d: error: stray \"%s\" in source", curfile, lineno, yytext); exit(1); return 0; }
%%

