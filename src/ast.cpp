#include <cassert>
#include "logging.h"
#include "ast.h"

void GlueOperator::traverseTree(std::vector<ASTNode*> &c, ASTNode* root)
{
    if (root->getType() != this->getType()) {
        c.push_back(root);
        return;
    }
    GlueOperator *g = dynamic_cast<GlueOperator*>(root);
    assert(g);
    traverseTree(c, g->left);
    traverseTree(c, g->right);
}

